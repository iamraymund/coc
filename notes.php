// NOTE:40 Clone git repo (bitbucket.org)
// SSH: git clone git@bitbucket.org:iamraymund/aic.git
// HTTPS: git clone https://iamraymund@bitbucket.org/iamraymund/aic.git

// NOTE: API is in /index.php/api
// TODO:190 Recommendation: Implement Basic OAuth

// NOTE:0 December 17-29, 2016: Completed all features and initial UI

// DONE:100 Add a table list of all added books with 'book_id' and 'course_code'
// NOTE:30 2 option to add book_course. If existing, add the 'book_id', otherwise add new book details
// DONE:80 Minimum of 5 books, 2 books must be within 3 year publication period
// DONE:90 Dashboard for notication (all users) if not yet reach both book requirements
// DONE:70 Make a format for authors
// DONE:130 Have an option to change role to chairperson / faculty
// DONE:140 Have an option to change role to faculty

// DONE:160 Make a single controller for every user
// DONE:150 Make a navigation for dean

// DONE:50 Make a reports table for every course together with the books added (dean)
// DONE:60 Make a reports table for every course together with the books added (chairperson)

// DONE:0 Put toast in all CRUD operations
// TODO: Truncate courses name in dean and chairperson dash to 60 characters only.
