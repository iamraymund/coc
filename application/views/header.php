<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="Raymund Edgar S. Alvarez">

    <title>Clash of Clans</title>

  	<!--Let browser know website is optimized for mobile-->
  	<meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-3.3.7/dist/css/bootstrap.min.css'); ?>">

    <!-- Latest compiled and minified bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap-3.3.7/dist/css/bootstrap-theme.min.css'); ?>">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">

</head>

<body>
