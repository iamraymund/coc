<?php
  class Player_details extends CI_Controller{
    public function index(){
      require_once APPPATH."/third_party/api/coc_api.php";

      $coc = new COC();
      $player_tag = (empty($this->input->get('tag')) ? 'VPRYLLRQ' : $this->input->get('tag'));
      if($player_tag[0] != "#"){
        $player_tag = "#" . $player_tag;
      }
      $return_val = $coc->get_acct_details($player_tag);
      if($return_val->status != 200){
        $return_val = $coc->get_acct_details("#VPRYLLRQ");
      }
      $result = json_decode($return_val->output);
      $data['acct'] = $result;
      $data['troops'] = $result->troops;
      $data['heroes'] = $result->heroes;
      $data['spells'] = $result->spells;

      $this->load->view('header');
      $this->load->view('navigation');
      $this->load->view('player_details', $data);
      $this->load->view('footer');
      // put here the custom js
      $this->load->view('footer_cont');

      // https://mybot.run/forums/index.php?/topic/22481-clash-of-clans-icon-pack/ @Hecki
    }
  }

 ?>
