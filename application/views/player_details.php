<div class="col-md-3 col-sm-12">
  <br />
  <h4>Account Details</h4>
  <br />
  <form method="GET">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-tag"></i></span>
        <input type="text" class="form-control" placeholder="Tag" name="tag" value="<?php echo $acct->tag; ?>">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Change</button>
        </span>
    </div>
  </form>
  <div class="bs-callout bs-callout-blue">
    <h4>Name: <span><?php echo $acct->name; ?></span></h4>
  </div>
  <div class="bs-callout bs-callout-red">
    <h4>Experience Level: <span><?php echo $acct->expLevel; ?></span></h4>
  </div>
  <div class="bs-callout bs-callout-green">
    <h4>Trophies: <span><?php echo $acct->trophies; ?></span></h4>
  </div>
  <div class="bs-callout bs-callout-orange">
    <h4>Town Hall Level: <span><?php echo $acct->townHallLevel; ?></span></h4>
  </div>
</div>

<?php
  // Mapping of icons
  $icon = array(
      "Barbarian King" => "./assets/COC Icons/Army/Heroes/Barbarian King Level 20 and up.ico",
      "Archer Queen" => "./assets/COC Icons/Army/Heroes/Archer Queen Level 20 and up.ico",
      "Barbarian" => "./assets/COC Icons/Army/Elixir Troops/Barbarian/Barbarian Level 7.ico",
      "Archer" => "./assets/COC Icons/Army/Elixir Troops/Archer/Archer Level 7.ico",
      "Goblin" => "./assets/COC Icons/Army/Elixir Troops/Goblin/Goblin Level 6.ico",
      "Giant" => "./assets/COC Icons/Army/Elixir Troops/Giant/Giant Level 7.ico",
      "Wall Breaker" => "./assets/COC Icons/Army/Elixir Troops/Wall Breaker/Wall Breaker Level 6.ico",
      "Balloon" => "./assets/COC Icons/Army/Elixir Troops/Balloon/Balloon Level 7.ico",
      "Wizard" => "./assets/COC Icons/Army/Elixir Troops/Wizard/Wizard Level 6.ico",
      "Healer" => "./assets/COC Icons/Army/Elixir Troops/Healer/Healer Level 3 and 4.ico",
      "Dragon" => "./assets/COC Icons/Army/Elixir Troops/Dragon/Dragon Level 5.ico",
      "P.E.K.K.A" => "./assets/COC Icons/Army/Elixir Troops/P.E.K.K.A/P.E.K.K.A Level 5.ico",
      "Miner" => "./assets/COC Icons/Army/Elixir Troops/Miner/Miner Level 3 and 4.ico",
      "Baby Dragon" => "./assets/COC Icons/Army/Elixir Troops/Baby Dragon/Baby Dragon Level 1 to 4.ico",
      "Bowler" => "./assets/COC Icons/Army/Dark Elixir Troops/Bowler/Bowler Level 3.ico",
      "Golem" => "./assets/COC Icons/Army/Dark Elixir Troops/Golem/Golem Level 5.ico",
      "Hog Rider" => "./assets/COC Icons/Army/Dark Elixir Troops/Hog Rider/Hog Rider Level 6.ico",
      "Lava Hound" => "./assets/COC Icons/Army/Dark Elixir Troops/Lava Hound/Lava Hound Level 3.ico",
      "Minion" => "./assets/COC Icons/Army/Dark Elixir Troops/Minion/Minion Level 7.ico",
      "Valkyrie" => "./assets/COC Icons/Army/Dark Elixir Troops/Valkyrie/Valkyrie Level 3 and 4.ico",
      "Witch" => "./assets/COC Icons/Army/Dark Elixir Troops/Witch/Witch Level 3.ico",
      "Freeze Spell" => "./assets/COC Icons/Spells/Freeze.ico",
      "Healing Spell" => "./assets/COC Icons/Spells/Healing.ico",
      "Jump Spell" => "./assets/COC Icons/Spells/Jump.ico",
      "Lightning Spell" => "./assets/COC Icons/Spells/Lightning.ico",
      "Rage Spell" => "./assets/COC Icons/Spells/Rage.ico",
      "Earthquake Spell" => "./assets/COC Icons/Dark Spells/Earthquake.ico",
      "Haste Spell" => "./assets/COC Icons/Dark Spells/Haste.ico",
      "Poison Spell" => "./assets/COC Icons/Dark Spells/Poison.ico",
  );
?>

<div class="col-md-3 col-sm-12 text-center">
  <br />
  <h4>Elixir / Dark Elixir Troops</h4>
  <br />
  <?php
    foreach($troops as $troop){
      $icon_link = (isset($icon[$troop->name]) ? $icon[$troop->name]: '');
      echo "<div class='col-xs-6 text-center'>
              <img src='$icon_link'/>
              <div>" . $troop->name . "</div>
              <div class='label label-primary btn-block'>Lvl. " . $troop->level . "</div>
            </div>";
    }
  ?>
</div>

<div class="col-md-3 col-sm-12 text-center">
  <br />
  <h4>Heroes</h4>
  <br />
  <?php
    foreach($heroes as $hero){
      $icon_link = (isset($icon[$hero->name]) ? $icon[$hero->name]: '');
      echo "<div class='col-xs-6 text-center'>
              <img src='$icon_link'/>
              <div>" . $hero->name . "</div>
              <div class='label label-primary btn-block'>Lvl. " . $hero->level . "</div>
            </div>";
    }
   ?>
</div>

<div class="col-md-3 col-sm-12 text-center">
  <br />
  <h4>Spells</h4>
  <br />

<?php
  foreach($spells as $spell){
    $icon_link = (isset($icon[$spell->name]) ? $icon[$spell->name]: '');
    echo "<div class='col-xs-6 text-center'>
            <img src='$icon_link'/>
            <div>" . $spell->name . "</div>
            <div class='label label-primary btn-block'>Lvl. " . $spell->level . "</div>
          </div>";
  }
 ?>
</div>
