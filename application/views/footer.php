<footer class="footer">
  <!--div class="container">
    <div class="row col-lg-12">
        <h5 class="white-text">AdNU Library Course-Based Book Inventory System</h5>
        <p class="grey-text text-lighten-4">AdNU Book Inventory(ABI) is a web application for ADNU College faculties that allows them to add, edit, delete and view the recommended books, that their respective departments require them to list for their respective courses.</p>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © Copyright 2016 Ateneo de Naga University Developed by the Ateneo Innovation Center</a>
    </div>
  </div-->
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/jquery-3.1.1/jquery.min.js'); ?>" ></script>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url('assets/bootstrap-3.3.7/dist/js/bootstrap.min.js'); ?>"></script>
