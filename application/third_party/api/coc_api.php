<?php

class COC
{
	private $apiKey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjUwZGRmMjYzLTkyMmMtNGQxYy1iOWM4LTEwOWEwMjdlNjE5YyIsImlhdCI6MTQ4NTMyNTU0Nywic3ViIjoiZGV2ZWxvcGVyL2JiYTY0NTFkLTZlMzgtMGUwYS00YzU2LWYxOTFkYjFlZDAxOSIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjQ5LjE1MC45My41MiIsIjEyOC4xOTkuMjQzLjE0MSJdLCJ0eXBlIjoiY2xpZW50In1dfQ.ZjY6FuCYJZqQMcDHodrssLRz8FmgtxfjWPCPXZWtHve9sLR72UPLLZgK9-9pQXf8xSEZpEIJcBBJAa0eCs3-9Q";

	protected function api_request($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  			'authorization: Bearer '.$this->apiKey
		));
		$output = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		return json_encode(array("status" => $httpcode, "output" =>$output));
	}

	public function get_acct_details($player_tag){
		$json = $this->api_request("https://api.clashofclans.com/v1/players/". urlencode($player_tag));
		return json_decode($json);
	}
};

?>
